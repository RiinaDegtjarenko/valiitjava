package it.vali;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;





        import java.text.DateFormat;
        import java.text.SimpleDateFormat;
        import java.util.Calendar;
        import java.util.Date;

public class Main {

    public static void main(String[] args) {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss EEEE");

        Calendar calendar = Calendar.getInstance();

        // Move calendar to yesterday
        //  calendar.add(Calendar.DATE, -1);

        // Get current date of calendar which point to the yesterday now
        Date date = calendar.getTime();

        System.out.println(dateFormat.format(date));

        //       calendar.add(Calendar.YEAR, 1);
        date = calendar.getTime();
        System.out.println(dateFormat.format(date));

        // Prindi ekraanile selle aasta järele jäänud kuude esimese kuupäeva nädalapäevad
        System.out.println(calendar.get(Calendar.MONTH));

        // Selle aasta, selle kuu esimene kuupäev
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), 1);

        int monthsLeftThisYear = 11 - calendar.get(Calendar.MONTH);

        DateFormat weekdayFormat = new SimpleDateFormat(" dd.MM.yyyy EEEE");

//        for (int i = 0; i < monthsLeftThisYear; i++) {
//            calendar.add(Calendar.MONTH, 1);
//            date = calendar.getTime();
//            System.out.println(weekdayFormat.format(date));
//        }

        int currentYear = calendar.get(Calendar.YEAR);

        calendar.add(Calendar.MONTH, 1);

        while(calendar.get(Calendar.YEAR) == currentYear) {

            date = calendar.getTime();
            System.out.println(weekdayFormat.format(date));
            calendar.add(Calendar.MONTH, 1);
        }

    }
}
