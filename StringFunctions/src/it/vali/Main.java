package it.vali;

public class Main {

    public static void main(String[] args) {
	    String sentence = "Elas metsas Mutionu keset kuuski noori vanu";
	    //Leia üles esimene tühik, mis on tema indeks
        //Sümbolite indeksid tekstis algavad samamoodi indeksiga 0 nagu massiivis
         int spaceIndex = sentence.indexOf("m");
         // indexOf tagastab -1, kui otsitavad fraasi (sümbolit või sõna) ei leitud
        //indeksi (ehk kust sõna algab), kui fraas leitakse
        //inteksi loendamine algab nullis, mitte 1st algavad 0

        int secondSpaceIndex = sentence.indexOf(" ", spaceIndex + 1);
        System.out.println(spaceIndex);
        //Prindi välja kõigi tühikute indeksid lauses


        while (spaceIndex != -1)  {
            spaceIndex = sentence.indexOf(" ", spaceIndex + 1) ;
            System.out.println(spaceIndex);
        }
        spaceIndex = sentence.lastIndexOf(" ");

        while (spaceIndex != -1) {
            System.out.println(spaceIndex);
            spaceIndex = sentence.lastIndexOf(" ", spaceIndex - 1);

            spaceIndex = sentence.indexOf(" ", spaceIndex );
            secondSpaceIndex =sentence.indexOf("", spaceIndex +1);
            String secondWord = sentence.substring( spaceIndex +1, secondSpaceIndex);

            System.out.println(secondWord);

            //Leia esimene k-tähega algav sõna
            // et leiaks ka siis, kui see sõna on lause esimene sõna


            int kIndex = sentence.indexOf(" k") + 1;
            spaceIndex = sentence.indexOf( " ", kIndex + 1);
            String kWord = sentence.substring(kIndex, spaceIndex);

            System.out.println(kWord);

            // Leia mitu k-tähega algavat sõna on lauses (kodune ülesanne)







        }

        int kCounter = 0;
        int kIndex = sentence.indexOf( " k");
        while (kIndex != -1) {
            kIndex = sentence.indexOf(" k", kIndex + 1);
            kCounter++;
        }

        if (sentence.substring(0,1). equals("k")) {
            kCounter++;

        }
        System.out.printf(" K-tähega algavate sõnade arv lauses on %d%n", kCounter);
    }
}
