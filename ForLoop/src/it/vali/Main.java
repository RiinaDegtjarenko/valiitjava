package it.vali;

public class Main {

    public static void main(String[] args) {
        // For tsükkel os selline tsükkel, kus korduste tsükkel on teada.
        // Lõpmatu for tsükkel järgnevas koodis ( ; ; ) {
        //for ( ; ; ) {
        //System.out.println("Väljas on ilus ilm.");
        //for (int i = 0; i < ; i++) {
        //for ja i ja vajutab enter ja loob for tsükli automaatselt valmis//
        // int i =  0 => siin saab luua muutujaid ja neid algväärtustada
        // luuakse täisarv i , mille väärtus hakkkab tsükli sees muutuma
        // 2) i < 3 => tingimus, mis peab olema tõene, et tsükkel käivituks ja korduks
        // 3) i++ => tegevus, mida iga tsükli korduse lõpus korratakse
        // i++ on sama mis i=i+1
        // i-- on sama mis i=i-i

        for (int i = 0; i < 3; i++) {
            System.out.println("Väljas on ilus ilm.");


        }
        // Prindi ekraanile numbrid 1 kuni 10


        for (int i = 1; i <= 10; i++) {
            System.out.println(i);

            //või

        }
        for (int i = 1; i <= 11; i++) {
        }
        //Prindi ekraanile numbrid 24st 167
        //Prindi ekraanile numbrid 18st 3ni

        for (int i = 24; i <= 167; i++) {
            System.out.println(i);
        }


        for (int i = 18; i >= 3; i--) {
            System.out.println(i);
        }
        //prindi ekraanile numbrid 2 4 6 8 10
        System.out.println();
        for (int i = 2; i <= 10; i = i + 2) {
            System.out.println(i);
            // i = i + 2 => i += 2
            // i = - 4  => i-= 4
            // i = i * 3 => i *= 3 arvu iseendaga korrutamine mingi numbriga
            // i =  i/ 2  => i/= 2


            // Prindi ekraanile numbrid 10 kuni 20 ja 40 kuni 60


        }
        for (int i = 10; i <= 60; i++) {

            if (i <= 20 || i >= 40)
                System.out.println(i);

        }
        System.out.println();

        for (int i = 10; i <= 50; i++) {
        if (i % 3 == 0) {
            System.out.println(i);
        }
        }

        // Prindi kõik arvud , mis jaguvad 3ga vahemikus 10 kuni 50
        // a % 3 (protsendimärk leiab jäägi) NT on 4%3 =>1
        // 4 %3  => jääk 1
        //6 & 3 => jääk 0
        //if (a % 3 ==0)
    }
}