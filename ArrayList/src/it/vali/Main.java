package it.vali;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int[] numbers = new int[5];

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        // Lisa massiivi 5 numbrit
        numbers[0] = -4;
        numbers[1] = 2;

        // eemalda siit teine number
        numbers[1] = 0;

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        numbers[2] = -4;
        numbers[3] = 2;

        System.out.println();

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        numbers[4] =  numbers[3];
        numbers[3] = 5;

        System.out.println();
        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        System.out.println();

        numbers[3] = numbers[4];
        numbers[4] = 0;

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        int x = numbers[0];
        numbers[0] = numbers[3];
        numbers[3] = x;

        System.out.println();

        // Tavalisse arraylisti võin lisada ükskõik mis tüüpi elemente
        // aga elemente küsides sealt pean ma teadma, mis tüüpi element kus
        // täpselt asub ning pean siis selleks tüübiks küsimisel ka cast-ima
        // teisendama
        List list = new ArrayList();

        list.add("tere");
        list.add(23);
        list.add(false);

        double money = 24.55;
        Random random = new Random();

        list.add(money);
        list.add(random);

        int a = (int)list.get(1);
        String word = (String)list.get(0);


        int sum = 3 + (int) list.get(1);

        if(Integer.class.isInstance(a)) {
            System.out.println("a on int");
        }

        if(Integer.class.isInstance(list.get(1))) {
            System.out.println("listis indeksiga 1 on int");
        }

        if(String.class.isInstance(list.get(0))) {
            System.out.println("listis indeksiga 0 on string");
        }

        list.add(2, "head aega");

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        List otherList = new ArrayList();
        otherList.add(1);
        otherList.add("maja");
        otherList.add(true);
        otherList.add(1);
        otherList.add(2.342333);

        list.addAll(otherList);

        System.out.println();

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        if(list.contains(money)) {
            System.out.println("money asub listis");
        }
        System.out.printf("money asub listis indeksiga %d%n", list.indexOf(money));
        System.out.printf("44 asub listis indeksiga %d%n", list.indexOf(44));

        list.remove(money);

        System.out.println();

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        System.out.println(list.size());
        // kustutab
        list.remove(5);

        // Asendab
        list.set(1, "tore");

        System.out.println();

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }
}
