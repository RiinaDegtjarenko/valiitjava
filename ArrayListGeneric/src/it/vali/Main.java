package it.vali;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	    //Generic arraylist on selline kollektsioon,
        //kus objekti loomise hetele peame määrama , mis tüüpi
        // elemente see sisaldama hakkab
        List<Integer> numbers =new ArrayList<Integer>();

        numbers.add(Integer.valueOf("10"));
        numbers.add(10);
        numbers.add(Integer.valueOf("12"));
        numbers.add(2);
        for (int i = 0; i <numbers.size(); i++) {
            System.out.println(numbers.get(i));

        }

        List<String> words = new ArrayList<String>();
        words.add("Tere");
        words.add("Tsau");

        for (int i = 0; i <words.size(); i++) {
            System.out.println(words.get(i));

        }




    }
}
