package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	     // Küsime kasutajalst pin koodi , kui see on õige, siis ütleme "Õige pin!"
        // muul juhul Küsime uuesti. Kokku küsime kolm korda.

        String realPin = "1234";

//        for (int i = 0; i < 3 ; i++) {
//            System.out.println("Palun sisesta pin kood!");
//            Scanner scanner = new Scanner(System.in);
//            String enteredPin = scanner.nextLine();
//            if (enteredPin.equals(realPin)) {
//                System.out.println("Pin aktsepteeritud!");
//                break;



           // }

        //}

        //prindi välja 10 kuni 20 ja 40 kuni 60

        //continue jätab selle tsükli korduse katki ja läheb järgmise korduse juurde
            //break hüppab tsükklist välja
            for (int i = 10; i <=60 ; i++) {
            if (i>20 && i< 40) {
                continue; //kui siin oleks break siis prinditaks 1-20
            }
            System.out.println(i);
        }
    }
}