package it.vali;


        import java.io.BufferedReader;
        import java.io.FileNotFoundException;
        import java.io.FileReader;
        import java.io.IOException;
i
public class Main {

    public static void main(String[] args) {
        try {
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\input.txt");

            BufferedReader bufferedReader = new BufferedReader(fileReader);

            // readLine loeb iga kord välja kutsudes järgmise rea
            String line = bufferedReader.readLine();

            // Kui readLine avastab, et järgmist rida tegelikult ei ole,
            // siis tahastab see meetod null
            while (line != null) {
                System.out.println(line);
                line = bufferedReader.readLine();
            }


            bufferedReader.close();
            fileReader.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}










