package it.vali;

public class Main {
    // Meetodid on mingid koodi osad, mis grupeerivad mingit teatud kindlat funktsionaalsust, kõik funktsionaalsus
    // tehakse meetodi sees
    // Kui koodis on korduvaid koodi osasid, võiks mõelda, et äkki peaks nende kohta tegema eraldi meetodi
    // Kahte tüüpi meetodid

    public static void main(String[] args) {
        printHello();
        printHello(3);
        printText("Hello");


    }

    //Lisame meetodi mis prindib ekraanile Hello
    private static void printHello() {
        System.out.println("Hello");
    }

    //Lisame meetodi, mis prindib Hello etteantud arv kordi
    static void printHello(int howManyTimes) {
        for (int i = 0; i < howManyTimes; i++) {
            System.out.println("Hello");

        }
    }

    //Lisame meetodi, mis prindib etteantud teksti välja printText
    static void printText(String text) {
        System.out.println(text);


    }


    //Lisame meeetodi, mis prindib etteantud teksti välja ette antud arv kordi
    static void printText(String text, int howManyTimes) {
        for (int i = 0; i < howManyTimes; i++) {
            System.out.println(text);

        }

    }

    //Method OVERLOADING - meil on mitu meetodit sama nimega, aga erineva parameetrite kombitsatsiooniga
    //meetodi ülelaadimine

    static void printText(String text, int howManyTimes, boolean toUppercase) {
        for (int i = 0; i < howManyTimes; i++) {
            if (toUppercase) ;
            System.out.println(text.toUpperCase());

        }


    }
}