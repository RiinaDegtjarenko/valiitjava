package it.vali;

public class Main {

    public static void main(String[] args) {
        //int vaikeväärtus 0
        // boolean vaikeväärtus false
        // double vaikeväärtus 0.0
        //String vaikeväärtus null
        //float vaikeväärtus 0.0f
        //objektide (Monitor, FileWriter)vaikeväärtus null
        //int[] arvud null; null
        Monitor firstMonitor = new Monitor();
        firstMonitor.setDiagonal(-1000);

        firstMonitor.setDiagonal(200);
        System.out.println(firstMonitor.getDiagonal());
        System.out.println(firstMonitor.getYear());

    }
}
