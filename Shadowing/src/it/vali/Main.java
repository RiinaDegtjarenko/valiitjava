package it.vali;

public class Main {
    static int a =3;

    public static void main(String[] args) {

       int b = 7;
       System.out.println(b+a);
       a = 0;
        System.out.println(increasByA(10));

        //Shadowing ehk seepool defineeritud muutuja varjutab väljaspool oleva muutuja

        int a = 6;
        a = 1;

        System.out.println(b+a);
        Main.a =8; //Nii saa klassi muutuja a väärtust muuta
        a = 5; // Meetodis defineeritud a väärtuse muutmine

        System.out.println(increasByA(10));
        System.out.println(b + Main.a);
        System.out.println(b+a);

    }

    static int increasByA(int b) {

        return b + a;

    }
}
