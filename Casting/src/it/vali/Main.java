package it.vali;

public class Main {

    public static void main(String[] args) {
	// casting on teisendamine ühest arvu tüübist teise
        byte a = 3;
        //kui üks numbri tüüp mahub teise sisse, siis toimub automaatne teisendamine (ing. keeles implicit casting)

        short b = a;

        // kui üks numbritüüp ei pruugi mahtuda teise sisse siis peab kõigepealt ise veenduma , et see
        //mahub teise numbritüüpi ja kui mahub , siis seda teisendama (explict casting)

        short c = 300;
        byte d = (byte)c;

        System.out.println(d);

        long e = 1000000000000000L;
        int f = (int) e;

        System.out.println(f);

        long g = f;
        g = c;
        g = d;

        float h = 123.234F;
        double i = h;

        double j = 55.11111111111;
        float k = (float)j;
        System.out.println(k);

        double l = -12E50;
        float m = (float)1;

        //12E50 tähendab 2 korda 10 astmel 50
        System.out.println(m);

        int n = 3;
        double o = n;

        double p = 2.99;
        short q = (short) p;

        System.out.println(q);

        int r = 2;
        double s = 9;
        //int jagatud int = int
        //int jagatud double = double
        //double jagatud int = double
        //double + int = double

        System.out.println(r / (double)s);
        System.out.println(r / (float)s);

        float t = 12.55555F;
        double u = 23.55555;











    }
}
