package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String savedFirstName = "Riina";
        String savedLastName = "Degtjarenko";

        //Kõigepealt küsitakse külastaja nime
        // kui nimi on listis, siis öeldakse kasutajale
        //"Teretulemast Riina!"
        //ja küsitakse külastaja vanust
        //kui kasutaja on alaealine, teavitatakse teda, et "Sorry!" sisse ei pääase,
        // muul juhul öeldakse "Tere tulemast!"
        //Kui kasutaja ei olnud listis
        //küsitakse kasutajalt ka perekonna nime
        // kuui perekonnanimi on listis, siis öeldakse "Teretulemas!"
        //muul juhul öeldakse "Ma ei tunne sind!"

        System.out.println("Kuidas on sinu nimi?");
        Scanner scanner = new Scanner(System.in);
        String guestFirstName = scanner.nextLine();
        if (savedFirstName.equals(guestFirstName)) {
            System.out.println("Tere %s%n", guestFirstName);
            System.out.println("Kui vana sa oled?");

            int age = Integer.parseInt(scanner.nextLine());
            if (age >= 18) {
                System.out.println("Teretulemast!");

            } else {
                System.out.println("Teretulemast!");
            } else {
            System.out.println("Sorry!, oled alaealine!");
        } else {
            System.out.printf("Mis on su perekonna nimi?");
            String guestLastName = scanner.nextLine();
        }


    }

}
