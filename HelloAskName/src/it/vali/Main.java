
package it.vali;
// import tähendab, et antud klassile Main lisatakse ligipääs Java class libariary paketile
//java.util paiknevale klassile Scanner
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // Loome Scanner tüübist nimega objekti scanner, mille kaudu saab kasutaja sisendit lugeda
        Scanner scanner = new Scanner(System.in);

        System.out.println("Tere, mis on sinu nimi?");
        String name = scanner.nextLine();

        System.out.println("Mis on su perekonna nimi?");
        String lastName = scanner.nextLine();

        System.out.println("Tere, " + name + " " + lastName);

        System.out.println("Mis on su lemmik värv?");
        String color = scanner.nextLine();

        System.out.printf("Oo, %s ilus värv on  %s!\n",//  \n tekitab reavahet
                name, color);


        StringBuilder builder = new StringBuilder();// teeb kõik ühele reale
        builder.append("Oo, ");
        builder.append(name);
        builder.append(" väga ilus värv on " );
        builder.append(color);

        System.out.println(builder.toString());

        // Nii System.ot.printf kui ka String.format kasutavad enda siseselt StringBuilderit

        String text = ("Oo, väga ilus värv on kollane!\n");
        System.out.println(text);












    }
}
