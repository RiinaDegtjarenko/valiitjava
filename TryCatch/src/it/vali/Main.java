package it.vali;

import com.sun.source.tree.CatchTree;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int a = 0;
        try {
            //String word = null;
            //word.length();
            int b = 4 / a;

        }
        //kui on mitu Catch plokki, siis otsib esimese ploki, mis oskab antud Exeptioni kinni püüda
        catch (ArithmeticException e) {
            if (e.getMessage().equals("/ by zero"))  {

                System.out.println("Nulli ei saa jagada!");
            }
            //
            // Exeptionid on klass, millest kõik erinevad Exeptioni tüübid pärinevad,
            // mis omakorda tähendab, püüdes kinni selle
            // üldise Exeptioni püüame kinni kõik Exeptionid, mis tähendab et kasutajani ei jõua ükski päris viga
            System.out.println("Esines aritmeetiline viga!");
            System.out.println(e.getMessage());

        }
        catch (RuntimeException e) {
            System.out.println("Reaalajas esines  viga!");
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Esines viga!");

        }



        //Küsime kasutajalt numbri ja kui ei ole korrektne, siis ütleme mingi veateate
        Scanner scanner = new Scanner(System.in);
        boolean correctNumber = false;
        do {
            System.out.println("Sisesta number!");

            try {
                int number = Integer.parseInt(scanner.nextLine());
                correctNumber = true;
            } catch (NumberFormatException e) {
                System.out.println("Sisesta päris number!");
                correctNumber = true;
                int number = Integer.parseInt(scanner.nextLine());
            }
            System.out.println("Viga see ei ole number! Proovi uuesti!");


        } while (!correctNumber);
        int number = Integer.parseInt(scanner.nextLine());
        System.out.println("Tubli!");


        //Loo täisarvude massiiv ja ürita sinna lisada kuues täisarv. Näita veateadet!

        int[] numbers = new int[] {1,2,3,4,5};
        try {
            numbers[5] = 6;
        } catch (ArrayIndexOutOfBoundsException e) {

            System.out.println("Indeksit, kuhu tahtsid väärtust määrata, ei eksisteeri!");
        }


    }


}
