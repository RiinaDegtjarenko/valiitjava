package it.vali;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        try {
            FileReader fileReader = new FileReader("C:\\Users\\opilane\\Documents\\input.txt");

            BufferedReader bufferedReader = new BufferedReader(fileReader);

            // readLine loeb iga kord välja kutsudes järgmise rea
            String line = bufferedReader.readLine();

            // Kui readLine avastab, et järgmist rida tegelikult ei ole,
            // siis tahastab see meetod null
            while (line != null) {
                System.out.println(line);
                line = bufferedReader.readLine();
            }


            bufferedReader.close();
            fileReader.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}


import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        //try plokis otsitakse või oodatakse exeptionit(erind või viga laias laastus)
        try {
            // FileWriter on selline klass, mis tegeleb faili kirjuatamisega, sellest klassist objekti
            //loomisel antakst talle ette faili asukoht.Faili asukoht võib olla ainult faili nime kirjeldatud:output.txt
            // sel juhul kirjeldatakse faili, mis asub samas kaustas, kus Main.class
            // või täispika asukohaga c:\\users\\opilane\\documents\\output.text
            FileWriter fileWriter = new FileWriter("c:\\users\\opilane\\Documents\\output.txt");

            fileWriter.write(String.format("Elas metsas Mutionu%n" ));
            fileWriter.write(String.format("Elas metsas Mutionu" + System.lineSeparator() ));

            fileWriter.close();



            //Catch plokis püütakse kinni kindlat tüüpi exeption või kõik exeptionid, mis pärinevad antud Exeptionist


        } catch (IOException e) {

            //printStackTrack tähendab, et prinditakse välja meetodite välja kutsumise hierarhia/ajalugu
            //e.printStackTrace();
            System.out.println("Viga:antud failile ligipääs ei ole võimalik!");

        }

    }
}
