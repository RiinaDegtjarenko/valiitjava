package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String sentence = "Väljas on ilus, vihma ei saja ilm ja päike paistab";
        String[] words = sentence.split(" ja | |, ");
        //for (int i = 0; i < words.length; i++) {
        //System.out.println(words[i]);
            //}

            String newsentence = String.join(" ", words);
            System.out.println(newsentence);
            newsentence = String.join(", ", words);
            System.out.println(newsentence);
            newsentence = String.join(" ja ", words);
            System.out.println(newsentence);
            newsentence = String.join("\t", words);
            System.out.println(newsentence);


            //Escaping
            //String.out.println ("Juku ütles: \"Mulle meeldib suvi\");
            //split tükeldab stringi ette antud sümbolite kohal ja tekitab sõnade massiivi
            // | tähendab regulaaravaldises või
            // join
            newsentence = String.join("", words);
            System.out.println(newsentence);

            //Küsi kasutajalt rida numbreid nii, et paneb need numbrid kirja ühele reale eraldades tühikuga
            //seejärel liidab need numbrid kokku ja prindib vastuse kasutajalt

            System.out.println("Sisesta arvud, mida tahad omavahel liita, eraldades tühikuga");
            Scanner scanner = new Scanner(System.in);
            String numbersText = scanner.nextLine();
            String[] numbers  = numbersText.split(" ");
            int summa = 0;
            for (int i = 0; i < numbers.length ; i++) {
               // summa= summa + Integer.parseInt(numbers[i]);
                summa += Integer.parseInt(numbers[i]);

            }
               String joinedNumbers =  String.join( ",", numbers);
                System.out.printf("Arvude %s summa on %d%n", joinedNumbers, summa);

    }

}














