package it.vali;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean wrongAnswer;
        String answer;
        // Küsi kasutajalt 2 arvu
        do {
            do {
                System.out.println("Sisesta esimene arv");


                int a = Integer.parseInt(scanner.nextLine());

                System.out.println("Sisesta teine arv");

                int b = Integer.parseInt(scanner.nextLine());
                System.out.println("Vali tehe");
                System.out.println("a) liitmine");
                System.out.println("b) lahutamine");
                System.out.println("c) korrutamine");
                System.out.println("d) jagamine");

                answer = scanner.nextLine();
                wrongAnswer = false;
    //            if( answer.equals("a")) {

    //                System.out.printf("Arvude %d ja %d summa on %d%n", a, b, sum(a, b));
    //            }
    //            else if (answer.equals("b")){
    //
    //                System.out.printf("Arvude %d ja %d vahe on %d%n", a, b, substract( a, b));
    //            }
    //            else if (answer.equals("c")){
    //
    //                System.out.printf("Arvude %d ja %d korrutis on %d%n", a, b, multiply(a, b));
    //
    //            }
    //            else if (answer.equals("d")){
    //                System.out.printf("Arvude %d ja %d jagatis on %.2f%n", a, b, divide(a, b));
    //
    //            }
    //            else {
    //                System.out.println(("Selline tehe puudub"));

               // }
                //SwitchCase konstruktsioon sobib kasutamiseks if ja else if asemel siis, kui if ja else if kontrollivad
                //ühe ja sama muutuja väärtust


                //System.out.println("Kas tahad veel arvutada?");

                switch (answer) {
                    case "A":
                    case "a":
                        System.out.printf("Arvude %d ja %d summa on %d%n", a, b, sum(a, b));
                        break;
                    case "B":
                    case "b":
                        System.out.printf("Arvude %d ja %d vahe on %d%n", a, b, substract(a, b));
                        break;
                    case "C":
                    case "c":
                        System.out.printf("Arvude %d ja %d korrutis on %d%n", a, b, multiply(a, b));
                        break;
                    case "D":
                    case "d":
                        System.out.printf("Arvude %d ja %d jagatis on %d%n", a, b, divide(a, b));
                        break;
                    default:
                        System.out.println(("Selline tehe puudub"));
                        wrongAnswer = true;
                        break;


                }

            }
            while (wrongAnswer);
            System.out.println(("Kas tahad jätkata? Jah/ei"));
            answer = scanner.nextLine();

        } while (!answer.equals("Ei"));
    }


    // Seejärel küsi kasutajalt, mis tehet ta soovib teha:
    // Vali tehe:

    // a)liitmine,
    static int sum(int a, int b) {
        int sum = a + b;
        return  sum;
    }


    // b)lahutamine,
    static int substract(int a, int b) {
        int sum = a - b; //võib ilma selleta kirjuatda otse  return a - b;
        return a - b;
    }

    // c)korrutamine,
    static int multiply(int a, int b) {
        int sum = a * b;
        return a * b;
    }


    // d)jagamine

    static double divide(int a, int b) {
        return (double)a / b;

        //Prindi kasutajale tehete vastus


    }



}


