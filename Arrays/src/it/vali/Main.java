package it.vali;


public class Main {

    public static void main(String[] args) {
        // Järjend, massiiv, nimikiri

        // Luuakse täisarvude massiiv, millesse mahub 5 elementi
        // Loomise hetkel määratud elementide arvu hiljem muuta ei saa.
        int[] numbers = new int[5];

        // Massiivi indeksid algavad 0st mitte 1st
        // viimane indeks on alati 1 võrra väiksem kui massiivi pikkus
        numbers[0] = 2;
        numbers[1] = 7;
        numbers[2] = -2;
        numbers[3] = 11;
        numbers[4] = 1;

        System.out.println(numbers[0]);
        System.out.println(numbers[1]);
        System.out.println(numbers[2]);
        System.out.println(numbers[3]);
        System.out.println(numbers[4]);

        System.out.println();

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }

        System.out.println();

        // Prindi numbrid tagurpidises järjekorras

        for (int i = 4; i >= 0 ; i--) {
            System.out.println(numbers[i]);
        }
        System.out.println();
        // Prindi numbrid, mis on suuremad kui 2

        for (int i = 0; i < numbers.length; i++) {
            if(numbers[i] > 2) {
                System.out.println(numbers[i]);
            }
        }
        System.out.println();
        // Prindi kõik paarisarvud

        for (int i = 0; i < numbers.length; i++) {
            if(numbers[i] % 2 == 0) {
                System.out.println(numbers[i]);
            }
        }
        System.out.println();
        // Prindi tagant poolt 2 esimest paaritut arvu
        int counter = 0;
        for (int i = numbers.length - 1; i >= 0 ; i--) {
            if(numbers[i] % 2 != 0) {
                System.out.println(numbers[i]);
                counter++;
                if(counter == 2) {
                    break;
                }
            }
        }

        // Loo teine massiiv 3-le numbrile ja pane sinna esimesest
        // massiivist 3 esimest numbrit.
        // Prindi teise massiivi elemendid ekraanile

        // Loo kolmas massiv 3-le numbrile ja pane sinna esimesest massiivist
        // 3 numbrit tagant poolt alates (1, 11, -2)
    }
}
