// public tähendab, et klass, meetod
// 		või muutuja on avalikult nähtav/ligipääsetav
// class - javas nagu üksus, üldiselt on ka eraldi fail,
//			mis sisaldab/grupeerib mingit funktsionaalsust
// HelloWorld on klassi nimi, mis on ka faili nimi
// static - meetodi ees tähendab, et seda meetodit saab
//   		välja kutsuda ilma
//			ilma klassist objekti loomata
// void - meetod ei tagasta midagi
//			meetodile on võimalik kaasa anda parameetrid,
//			mis pannakse sulgude sisse, eraldades komaga
//string[] - tähistab stringi massiivi
// args - massiiivi nimi, sisaldab käsurealt kaasa pandud parameetreid
// System.out.println - on java meetod, millega saab printida välja teksti
//						see, mis kirjutatakse sulgudesse, prinditakse välja ja tehakse rea vahetus



public class Main {

    public static void main(String[] args) {
	    System.out.println("Hello World");
    }
}
