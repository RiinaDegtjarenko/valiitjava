package it.vali;

import java.util.ArrayList;
import java.util.List;

enum Fuel {GAS, PETROL, DIESEL, HYBRIDE, ELECTRIC
}

public class Car {
    private  String make;
    private  String model;
    private  int year;
    private  Fuel fuel;
    private  boolean isUsed;

    private boolean isEngineRunning;
    private  int speed;
    private  int maxSpeed;
    private int  minSpeed;
    private Person driver;
    private Person owner;

    private List<Person> passengers
            = new ArrayList<Person>();




    // konstruktor - constructor - on eriline meetod, mis käivitatakse klassi objekti loomisel,
    // saad anda parameetrid kaasa, kutsutakse välja siis kui ma objekti loon
    //constructor luuakse automaatselt, kuid kui lood käsitsi automaatne siis programm kaob
    public Car() {
        System.out.println("Loodi auto objekt");
        maxSpeed = 200;
        isUsed = true;
       fuel = Fuel.PETROL;
    }
        //Constructor overloading

    public Car(String make, String model, int year, int maxSpeed) {
        this.make = make;
        this.make = model;
        this.year = year;
        this.maxSpeed = maxSpeed;


    }
    public void startEngine() {
        if ((!isEngineRunning)) {
            isEngineRunning =true;
            System.out.println("Mootor käivitus");

        } else {
            System.out.println("Mootor juba töötab");

        }

    }

  public void stopEngine() {
      if ((isEngineRunning)) {
          isEngineRunning = false;
          System.out.println("Mootor seiskus");

      } else {
          System.out.println("Mootor ei töödanudki");
      }
  }

  public void accelerate(int targetSpeed) {
        if (!isEngineRunning) {
            System.out.println("Auto mootor ei tööta, ei saa käivitada");

        } else if (targetSpeed > maxSpeed) {
            System.out.println("Auto nii kiiresti ei sõida");
            System.out.printf("Auto maksimum kiirus on %d%n", maxSpeed);

        } else if (targetSpeed < 0) {
            System.out.println("Auto kiirus ei saa olla negatiivne");

        } else if (targetSpeed < speed) {
            System.out.println("Auto ei saa kiirendada väiksemale kiirusele");

        }else {
            System.out.printf("Auto kiirendas kuni kiiruseni %d%n,", targetSpeed);
        }

  }
    // slowDown(int TargetSpeed)
  public void slowDown(int targetSpeed) {
        if (!isEngineRunning) {
            System.out.println("Auto mootor ei tööta, ei saa aeglustada");
        }else if (targetSpeed > minSpeed) {
            System.out.println("Auto kiirus väheneb");
            System.out.printf("Auto min kiirus on %d%n", minSpeed);
        }else if (targetSpeed > 0){
            System.out.println("Auto seiskus");
        }else {
            System.out.printf("Auto vähendas kiirust kuni kiiruseni %d%n,", minSpeed);
        }

}


//park() mis tegevused oleks vaja teha (kutsu välja juba olemasolevaid meetodeid
    public void park() {
        slowDown(0);
        stopEngine();
    }
}


//Lisa autole parameetrile driver ja owner (tüübist Person) ja nendele siis get ja set meetodid
//Loo mõni autoobjekt, kellel on siis määratud kasutaja ja omanik
//prindi välja auto omaniku vanus