package it.vali;
enum Gender{
    FEMALE,MALE,NOTSPECIFIED
}

public class Person {
    private  String firstName;
    private  String lasttName;
    private  Gender gender;
    private  int age;

    //Kui klasssil ei ole defineeritud konstruktorit,siis tegelikult tehakse
    // nähtatamatu parameetrita konstruktor, mille sisu on tühi
    // kui klassile ise lisada mingi konstruktor siis nähtamatu parameetrita konstruktor kustutatakse
    //sellest klassist saab teha objekti ainult selle uue konstruktoriga

    public Person(String firstName,String lasttName, Gender gender ,int age){
        this.firstName = firstName;
        this.lasttName = lasttName;
        this.gender = gender;
        this.age = age;

    }
}
